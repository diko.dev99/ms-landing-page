const { NEXT_PUBLIC_API_HOST } = process.env
import axios from 'axios'
import errorHandler from 'configs/axios/errorHandler'

const instance = axios.create({
  baseURL: NEXT_PUBLIC_API_HOST
})

instance.interceptors.request.use((response) => response.data, errorHandler)

export default instance