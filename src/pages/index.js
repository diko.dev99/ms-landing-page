import Head from "next/head";
import Link from "next/link";

import axios from 'configs/axios'

function Home({data}) {
  console.log(data)
  return (
    <div className="container mx-auto mt-4">
      <Head>
        <title>Coco | Microservice </title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <Link href="/random">
          <a>Random</a>
        </Link>
        <h1 className="text-center">Selamat datang di halaman utaman</h1>
      </main>
    </div>
  );
}

Home.getInitialProps = async() => {
  try {
    const data = axios.get('/courses')
    return {data}
  } catch (error) {
    return error
  }
}

export default Home