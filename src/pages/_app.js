import Router from "next/router";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
import "../../tailwindcss/styles.css"
import 'react-toastify/dist/ReactToastify.css'
import {ToastContainer} from 'react-toastify'


NProgress.configure({}) //show spinner false
Router.events.on("routeChangeStart", () => NProgress.start());
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());


function MyApp({ Component, pageProps }) {
    return (
        <>
            <Component {...pageProps} />
            <ToastContainer position="top-center" />
        </>
    )
}


export default MyApp