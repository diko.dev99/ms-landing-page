import Head from 'next/head'
import Link from 'next/link'

function Random({ data }) {
  return (
    <>
      <Head>
        <title>Microservice | Random</title>
      </Head>
      
      <main>
        <h1 className="text-center">
          Info random halaman
        </h1>
        <ul className="border border-indigo-700 mx-auto">
          {
            data.map(todo => {
              return <li key={todo.id} className="border border-indigo-700">
                {todo?.title ?? "-"} <Link href="/random/[id]" as={`/random/${todo.id}`}><a>Launch</a></Link>
              </li>
            })
          }
        </ul>
        <Link href="/">
          <a>back to home</a>
        </Link>
      </main>
    </>
  )
}

export default Random
