import Head from 'next/head'
import Link from 'next/link'

function Todo({ data }) {
  return (
    <>
     <Head>
       <title>
         Micro | Detail Todos | {data.title} 
       </title>
      </Head> 

      <div className="container ml-10 mt-5">
        <h1 className="text-3xl">{data.title}</h1>
        <h1 className="text-2xl">completly task</h1>
        <Link href="/random">back to home</Link>
      </div>
    </>
  )
}

export default Todo
